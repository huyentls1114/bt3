<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Insert title here</title>
	<script src="https://code.jquery.com/jquery-3.2.1.js"></script>
	<script src="jquery.validate.min.js" /></script>
	 <script language="javascript">
            function validateForm()
            {
            	b=$('#birthday');
    			c=new Date(b.val());
    			today=new Date();
    			if (today.getTime()-c.getTime()<18*24*3600000*365){
    				alert("Invalid birthday");
                	return false;
    			}
    			else{
    				alert("Sucessfull");
    				return true;
    			}
    				
            }
        </script>
</head>
<body>
	<form method="POST" action='UserController' name="frmAddUser" id="formUser" onsubmit="return validateForm()">
        User Id : <input type="text" readonly="readonly" name="userid" id="userid" value="<c:out value="${user.userid}" />" /> <br> 
        Username : <input type="text" name="username" id="username" value="<c:out value="${user.username}" />" /><br /> 
        Password : <input type="text" name="password" id="password" value="<c:out value="${user.password}" />" /><br /> 
        Name : <input type="text" name="name" id="name" value="<c:out value="${user.name}" />"  /><br /> 
        Adress : <input type="text" name="adress" id="adress" value="<c:out value="${user.adress}" />"  /><br /> 
        Birthday : <input type="text" name="birthday" id="birthday" value="<fmt:formatDate pattern="dd/MM/yyyy" value="${user.birthday}" />" /> <br /> 
        Married : 
        	<select size="1" name="married"">
        		<option value=true  selected="${user.married}">Yes</option>
        		<option value=false  selected="${user.married}">No</option>
        	</select><br /> 
        Job: <input type="text" name="job" value="<c:out value="${user.job}" />"  /><br />
        <input type="submit" value="Submit" />
    </form>
	
</body>
</html>