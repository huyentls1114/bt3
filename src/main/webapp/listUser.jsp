<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
</head>
<body>
    <table class="table table-bordered">
        <thead>
            <tr class="success">
                <th>User Id</th>
                <th>Name</th>
                <th>Adress</th>
                <th>Birthday</th>
                <th>Married</th>
                <th>Job</th>
                <th colspan=2>Action</th>
            </tr>
        </thead>
        <tbody>
            <c:forEach items="${users}" var="user">
                <tr class="warning">
                    <td><c:out value="${user.userid}" /></td>
                    <td><c:out value="${user.name}"/></td>
                    <td><c:out value="${user.adress}" /></td>
                    <td><fmt:formatDate pattern="yyyy-MMM-dd" value="${user.birthday}" /></td>
                    <td><c:out value="${user.married}" /></td>
                    <td><c:out value="${user.job}" /></td>
                    <td><a href="UserController?action=edit&userid=<c:out value="${user.userid}"/>">Update</a></td>
                    <td><a href="UserController?action=delete&userid=<c:out value="${user.userid}"/>">Delete</a></td>
                </tr>
            </c:forEach>
        </tbody>
    </table>
    <button class="btn btn btn btn-primary"><a href="UserController?action=insert" style="color:white">Add User</a></button>
    <button type="button" class="btn btn btn-primary"><a href="LogoutController" style="color:white" >Log out</a>
</body>
</html>