package com.huyen.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import com.huyen.model.User;
import com.huyen.util.DbUtil;

public class UserDao {
	private Connection connection;
	public UserDao(){
		connection= DbUtil.getConnection();
	}
	public void addUser(User user){
		try {
			PreparedStatement preparedStatement=connection.prepareStatement("insert into users(username, password, name, adress, birthday, job, married) value(?,?,?,?,?,?,?)");
			preparedStatement.setString(1, user.getUsername());
			preparedStatement.setString(2, user.getPassword());
			preparedStatement.setString(3, user.getName());
			preparedStatement.setString(4, user.getAdress());
			preparedStatement.setDate(5, new Date(user.getBirthday().getTime()) );
			preparedStatement.setString(6, user.getJob());
			preparedStatement.setBoolean(7, user.getMarried());
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
	public void deleteUser(int userid){
		try {
			PreparedStatement preparedStatement=connection.prepareStatement("delete from users where userid=?");
			preparedStatement.setInt(1, userid);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
	public void updateUser(User user){
		try {
			PreparedStatement preparedStatement=connection.prepareStatement("update users set username=?, password=?, name=?, adress=?, birthday=?, job=?, married=?" + " where userid=?");
			preparedStatement.setString(1, user.getUsername());
			preparedStatement.setString(2, user.getPassword());
			preparedStatement.setString(3, user.getName());
			preparedStatement.setString(4, user.getAdress());
			preparedStatement.setDate(5, new Date(user.getBirthday().getTime()));
			preparedStatement.setString(6, user.getJob());
			preparedStatement.setBoolean(7, user.getMarried());
			preparedStatement.setInt(8, user.getUserid());
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
	public List<User> getAllUser(){
		List<User> users=new ArrayList<User>();
		try {
			Statement statement=connection.createStatement();
			ResultSet rs=statement.executeQuery("select * from users");
			while(rs.next()){
				User user=new User();
				user.setUserid(rs.getInt("userid"));
				user.setUsername(rs.getString("username"));
				user.setPassword(rs.getString("password"));
				user.setName(rs.getString("name"));
				user.setAdress(rs.getString("adress"));
				user.setBirthday(rs.getDate("birthday"));
				user.setJob(rs.getString("job"));
				user.setMarried(rs.getBoolean("married"));
				users.add(user);
			}
		} catch (SQLException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return users;
	}
	public User getUserById(int userid){
		User user=new User();
		try {
			PreparedStatement preparedStatement=connection.prepareStatement("select * from users where userid=?");
			preparedStatement.setInt(1, userid);
			ResultSet rs=preparedStatement.executeQuery();
			if(rs.next()){
				user.setUserid(rs.getInt("userid"));
				user.setUsername(rs.getString("username"));
				user.setPassword(rs.getString("password"));
				user.setName(rs.getString("name"));
				user.setAdress(rs.getString("adress"));
				user.setBirthday(rs.getDate("birthday"));
				user.setJob(rs.getString("job"));
				user.setMarried(rs.getBoolean("married"));
			}
		} catch (SQLException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return user;
	}
	public Boolean checkUsernamePassword(String username,String password){
		User user=new User();
		try {
			PreparedStatement preparedStatement=connection.prepareStatement("select * from users where username=?");
			preparedStatement.setString(1, username);
			ResultSet rs=preparedStatement.executeQuery();
			if(rs.next()){
				if(rs.getString("password").equals(password) )
					return true;
				else
					return false;
				}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return false;
	}
	public static void main(String[] args) {
		UserDao dao=new UserDao();
		User user=new User();
		user.setUserid(5);
		user.setAdress("a");
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		java.util.Date date = new Date(0);
		user.setBirthday(date);
		user.setMarried(true);
		user.setJob("1");
		user.setName("1");
		user.setUsername("1");
		user.setPassword("111");
		dao.updateUser(user);
		List<User> users=dao.getAllUser();
		for (User user1:users){
			System.out.println(user1.toString());
		}
	}
}
