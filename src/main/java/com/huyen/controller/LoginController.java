package com.huyen.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.huyen.dao.UserDao;
import com.huyen.model.User;

/**
 * Servlet implementation class LoginController
 */
public class LoginController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static String LIST_USER="/listUser.jsp";
	private static String LOGIN="/login.jsp";
    private UserDao dao;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginController() {
        super();
        // TODO Auto-generated constructor stub
        dao=new UserDao();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String forward=LOGIN;
		RequestDispatcher view=request.getRequestDispatcher(forward);
		view.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String username=request.getParameter("username");
		String password=request.getParameter("password");
		if(dao.checkUsernamePassword(username, password)){
				Cookie ck=new Cookie("name", username);
				response.addCookie(ck);
				request.setAttribute("users", dao.getAllUser());
				RequestDispatcher view=request.getRequestDispatcher("/listUser.jsp");
				view.forward(request, response);
		}
		else{
			response.setContentType("text/html");  
	        PrintWriter out=response.getWriter(); 
	        out.print("Incorrect username or password! Please try again");  
	        request.getRequestDispatcher("./login.jsp").include(request, response);  
		}
	}

}
