package com.huyen.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.huyen.dao.UserDao;
import com.huyen.model.User;

/**
 * Servlet implementation class UserController
 */
public class UserController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static String INSERT_OR_EDIT="/user.jsp";
	private static String LIST_USER="/listUser.jsp";
	private UserDao dao;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserController() {
        super();
        // TODO Auto-generated constructor stub
        dao=new UserDao();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Cookie ck[]=request.getCookies();
		if(ck!=null){
			String forward="";
			String action=request.getParameter("action");
			if(action.equalsIgnoreCase("delete")){
				int userid=Integer.parseInt(request.getParameter("userid"));
				dao.deleteUser(userid);
				forward=LIST_USER;
				request.setAttribute("users", dao.getAllUser());
			}
			else if(action.equalsIgnoreCase("edit")){
				forward=INSERT_OR_EDIT;
				int userid=Integer.parseInt(request.getParameter("userid"));
				User user=dao.getUserById(userid);
				request.setAttribute("user", user);
			}
			else if(action.equalsIgnoreCase("listUser")){
				forward=LIST_USER;
				request.setAttribute("users", dao.getAllUser());
			}
			else
				forward=INSERT_OR_EDIT;
			RequestDispatcher view=request.getRequestDispatcher(forward);
			view.forward(request, response);
		}
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		User user=new User();
		user.setUsername(request.getParameter("username"));
		user.setPassword(request.getParameter("password"));
		user.setName(request.getParameter("name"));
		user.setAdress(request.getParameter("adress"));
		try {
            java.util.Date birthday = new SimpleDateFormat("MM/dd/yyyy").parse(request.getParameter("birthday"));
            user.setBirthday(birthday);
        } catch (ParseException e) {
            e.printStackTrace();
        }
		Boolean married = Boolean.parseBoolean(request.getParameter("married"));
		user.setMarried(married);
		user.setJob(request.getParameter("job"));
		String userid=request.getParameter("userid");
		
        if(userid==null|| userid.isEmpty())
        	dao.addUser(user);
        else{
        	user.setUserid(Integer.parseInt(userid));
        	dao.updateUser(user);
        }
        RequestDispatcher view = request.getRequestDispatcher(LIST_USER);
        request.setAttribute("users", dao.getAllUser());
        view.forward(request, response);
	}

}
