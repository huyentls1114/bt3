package com.huyen.util;

import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class DbUtil {
	public static Connection connection=null;
	public static Connection getConnection(){
		if(connection!=null)
			return connection;
		else{
			try {
				Properties prop=new Properties();
				InputStream inputStream=DbUtil.class.getClassLoader().getResourceAsStream("db.properties");
				prop.load(inputStream);
				String driver= prop.getProperty("driver");
				String url = prop.getProperty("url");
				String user=prop.getProperty("user");
				String password=prop.getProperty("password");
				Class.forName(driver);
				inputStream.close();
				connection=DriverManager.getConnection(url, user, password);
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
				// TODO: handle exception
			} catch (SQLException e) {
				// TODO: handle exception
				e.printStackTrace();
			}catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			} 
			if (connection != null) {
				System.out.println("You made it, take control your database now!");
			} else {
				System.out.println("Failed to make connection!");
			}
			return connection;
		}
	}
}
